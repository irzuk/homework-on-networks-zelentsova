# Networks_HW_2

Был реализован REST Service на языке Java с помощью Spring.
Сначала получили все тестовые данные

![alt text](screenshots/postman1.png)
Изменили по id 3 название и описание

![alt text](screenshots/postman2.png)
Удалили 3 элемент и еще раз получили весь список

![alt text](screenshots/postman3.png)

_______
Работа с логотипом:

Загрузка продукта с описанием и логотипом при создании

![alt text](screenshots/postman4.png)

Скачивание картинки конкретного продукта
![alt text](screenshots/postman5.png)

